/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlversiones04;

import java.util.Scanner;

/**
 *
 * @author HARRY
 */
public class ControlVersiones04 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int opcion;
        Casa c1 = null;
        Scanner teclado = new Scanner(System.in);

        do 
        {
            System.out.println("****MENU*****");
            System.out.println("1. Crear casa");
            System.out.println("2. Mostrar metros");
            System.out.println("3. Modificar descripcion");
            System.out.println("4. Mostrar informacion de la casa");
            System.out.println("5. Salir");
            System.out.print("Introducir una opcion deseada: ");
            opcion = teclado.nextInt();
            switch (opcion)
            {
                case 1:
                    System.out.print("Metros de la cocina: ");
                    
                    double cocina = teclado.nextDouble();
                    System.out.print("¿es independiente? Escribir s/n: ");
                    char letra = teclado.next().toLowerCase().charAt(0);
                    boolean independiente = false;
                    if (letra == 's') 
                    {
                        independiente = true;
                    }
                    System.out.print("Metros del salon: ");
                    double salon = teclado.nextDouble();
                    c1 = new Casa(salon,cocina,independiente,"");                 
                    
                    
                    break;
                case 2:
                    if (c1 != null)
                    {
                        System.out.println(c1.mostrarMetros());
                    } else
                    {
                        System.out.println("No hay ningun dato creado, vaya a la opcion 1 primero");
                    }
                    break;
                case 3:
                    if (c1 != null) 
                    {
                        System.out.print("introduzca descripcion:");
                        teclado.nextLine();
                        String descripcion= teclado.nextLine();
                        c1.getSalon().setDescripcion(descripcion);
                                
                    } 
                    else 
                    {
                        System.out.println("No hay ningun dato creado, vaya a la opcion 1 primero");
                    }
                    break;
                case 4:
                    if (c1 != null)
                    {
                        System.out.print(c1);
                    } 
                    else 
                    {
                        System.out.println("No hay ningun dato creado, vaya a la opcion 1 primero");
                    }
                    break;
                case 5:
                    System.out.println("¡Gracias por visitarnos!");
                    break;
                default:
                    System.out.println("Error");
            }
        } while (opcion != 5);

    }
    
    
    
    
}
